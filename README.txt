Exercício 2: Detector de Ortografia

Utilizando:

Scala - 2.11.7
SBT
ScalaTest - TDD
IntelliJ
algebird-core - Utilizando o Bloom-Filter ao invés de HashSet em Memória
O tempo de carga do Bloom-Filter é bem mais demorado (leva 2 minutos ou mais) que usar HashSet (rápido),
porém ele usa menos memória e é mais rápido nas buscas

dicionário usado:
/usr/share/dict/portuguese (enviado em anexo no email) - Encoding = iso-8859-1
Se não estiver disponível no S.O (Linux), favor usarem a base enviada no email e colocar nesse diretório: /usr/share/dict

Para rodar
- Efetuar clone
- Executar script.sh:

Ex: ./script.sh "Nós fui jogarmos futebol"

Nota: o dicionário (no meu PC) não possui a palavra Nós, logo a frase acima gera 1 erro

name := "Ortografica"

version := "1.0"

scalaVersion := "2.11.7"

libraryDependencies ++= Seq("com.twitter" % "algebird-core_2.11" % "0.11.0",
                            "org.scalatest" % "scalatest_2.11" % "2.2.4" % "test")

    
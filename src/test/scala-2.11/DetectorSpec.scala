import org.scalatest.{FlatSpec, ShouldMatchers}

import scala.collection.mutable

/**
 * Created by senaga on 05/09/15.
 */
class DetectorSpec extends FlatSpec with ShouldMatchers {

  val PROBABILITY = 0.01
  val FILENAME = "/usr/share/dict/portuguese"

  def createHashBaseInMemory(words: Array[String]): mutable.HashSet[String] = {
    val hash = mutable.HashSet[String]()
    words.foreach (hash.add(_))
    hash
  }

  def analyseHash(hash: mutable.HashSet[String], text: String) = {
    text.split("\\s+").filterNot(hash.contains(_)).length
  }

  println("Loading File...")
  val words = Detector.loadFile(FILENAME)
  println("Loading Bloom Filter...")
  val bf = Detector.createBloomFilter(PROBABILITY, words)
  println("Loading Hash...")
  val hash = createHashBaseInMemory(words)

  "analyse" should "return equals analyseHash given an João gosta de jiugar futchibol" in {
    val count = analyseHash(hash, "João gosta de jiugar futchibol")
    Detector.analyse(bf, "João gosta de jiugar futchibol") should equal(count)
  }

  "analyse" should "return equals analyseHash given an Nós fui jogarmos futebol" in {
    val count = analyseHash(hash, "Nós fui jogarmos futebol")
    Detector.analyse(bf, "Nós fui jogarmos futebol") should equal(count)
  }

  "analyse" should "return equals analyseHash given an Nós   fui   jogarmos    futebol" in {
    val count = analyseHash(hash, "Nós   fui   jogarmos    futebol")
    Detector.analyse(bf, "Nós   fui   jogarmos    futebol") should equal(count)
  }

  "analyse" should "return equals analyseHash given an O Rato Roeu a Roupa do Rei de Roma" in {
    val count = analyseHash(hash, "O Rato Roeu a Roupa do Rei de Roma")
    Detector.analyse(bf, "O Rato Roeu a Roupa do Rei de Roma") should equal(count)
  }
}

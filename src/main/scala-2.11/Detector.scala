import com.twitter.algebird._

/**
 * Created by senaga on 05/09/15.
 */
object Detector extends App {

  val PROBABILITY = 0.01
  val FILENAME = "/usr/share/dict/portuguese"

  def loadFile(filename: String): Array[String] = {
    val source = scala.io.Source.fromFile(filename, "iso-8859-1")
    try {
      source.getLines().toArray
    } finally {
      source.close()
    }
  }

  // Utilizando bloom-filter: https://en.wikipedia.org/wiki/Bloom_filter
  def createBloomFilter(prob: Double, words: Array[String]): BF = {
    val bloom = BloomFilter(words.length, prob)
    bloom.create(words:_*)
  }

  def analyse(bloomFilter: BF, text: String): Int = {
    text.split("\\s+").filterNot(bloomFilter.contains(_).isTrue).length
  }

  if (args.length != 1) {
    println( """Uso: ./script.sh "frase"\n""")
  } else {
    println("Creating bloom filter...")
    var bf = createBloomFilter(PROBABILITY, loadFile(FILENAME))
    println(analyse(bf, args(0)))
  }
}
